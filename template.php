<?php

/**
 * @file
 * template.php
 */

/**
 * Allows for images as menu items.
 * Just supply the image path in the title. The image path will be replaced
 * with an img tag. The description is used as alt text and title.
 * Implements theme_menu_link().
 */
function sfb_menu_link($variables) {
  $element = &$variables['element'];

  $pattern = '/\S+\.(png|gif|jpg|svg)\b/i';
  if (preg_match($pattern, $element['#title'], $matches) > 0) {
    $element['#title'] = preg_replace($pattern,
      '<img alt = "' . $element['#localized_options']['attributes']['title']
      . '" src = "' . url($matches[0]) . '" height="40" width="40" />',
      $element['#title']);
    $element['#localized_options']['html'] = TRUE;
  }

  return theme_menu_link($variables);
}

/**
 * Adds a css class to a given image style (trr_center_image)
 * The image style is not available by default. An admin has to create it on the
 * admin page.
 * @param $vars
 */
function sfb_preprocess_image_style(&$vars){
  if($vars['style_name'] == 'trr_center_image'){
    $vars['attributes']['class'][] = 'trr-center-image';
  }
}

/*
function sfb_image_default_styles() {
  $styles = array();
  $styles['mymodule_preview'] = array(
    'label' => 'My module preview',
    'effects' => array(
      array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 400,
          'height' => 400,
          'upscale' => 1,
        ),
        'weight' => 0,
      ),
      array(
        'name' => 'image_desaturate',
        'data' => array(),
        'weight' => 1,
      ),
    ),
  );
  return $styles;
}
*/
